//
//  Profile.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/29/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Profile : NSObject, NSCoding, NSCopying  {

    var userId      : String?
    var profileId   : String?
    var firstName   : String?
    var lastName    : String?
    var email       : String?
    var photoUrl    : String?

    required override init(){}

    required init(json: JSON) {
        self.userId    = json["userId"].stringValue
        self.profileId = json["profileId"].stringValue
        self.firstName = json["firstName"].string ?? ""
        self.lastName  = json["lastName"].string ?? ""
        self.email     = json["email"].stringValue
        self.photoUrl  = json["photoUrl"].stringValue
    }

    required convenience init(data: [String : AnyObject]) {
        self.init(json: JSON(data))
    }


    // MARK: MSCoding
    // -----------------------------

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userId, forKey: "userId")
        aCoder.encode(self.profileId, forKey: "profileId")
        aCoder.encode(self.first_name, forKey: "firstName")
        aCoder.encode(self.last_name, forKey: "lastName")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.photo, forKey: "photoUrl")
    }

    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.userId = aDecoder.decodeObject(forKey: "userId") as! String
        self.profileId = aDecoder.decodeObject(forKey: "profileId") as! String
        self.firstName = aDecoder.decodeObject(forKey: "firstName") as! String
        self.lastName = aDecoder.decodeObject(forKey: "lastName") as! String
        self.email = aDecoder.decodeObject(forKey: "email") as! String
        self.photoUrl = aDecoder.decodeObject(forKey: "photoUrl") as! String
    }

    public func copy(with zone: NSZone? = nil) -> Any {
        let user = Profile()
        user.userId = self.userId
        user.profileId = self.profileId
        user.firstName = self.firstName
        user.lastName = self.lastName
        user.email = self.email
        user.photoUrl = self.photoUrl

        return user
    }
}
