//
//  LoginProtocol.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/28/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import UIKit

public protocol MSAuthenticationProtocol: AnyObject {
    func loginDidSuccess(response: [String: AnyObject])
    func loginDidFail(error: [String: Any]?)
    func loginDidTimeout()

    func didTapForgotPassword()
    func didTapSignUp()
}

extension MSAuthenticationProtocol {
    func loginDidSuccess(response: [String: AnyObject]) {
        print("loginDidSuccess - Default implementation")
    }

    func loginDidFail(error: [String: Any]?) {
        print("loginDidFail - Default implementation ")
    }

    func loginDidTimeout() {
        print("loginDidTimeout - Default implementation")
    }
    
    func didTapForgotPassword() {
        print("didTapForgotPassword - Default implementation")
    }
    
    func didTapSignUp() {
        print("didTapSignUp - Default implementation")
    }

}
