//
//  Settings.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/28/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import UIKit


    /**
     Aqui se definiran los colores que se utilizaran en el modulo de login
     */
    


public struct MSAuthenticationSettings {

    public struct URLs {
        public static var baseURL: String = ""
        public static var login: String = baseURL + "login"

    }

}
