//
//  User.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/29/20.
//

import Foundation
import SwiftyJSON

public class User : NSObject {

    var profile : Profile?
    var token : String?

    required override init(){}

    required init(json: JSON) {
        self.profile  = Profile(json: JSON(json["profile"].dictionaryObject))
        self.token    = json["token"].stringValue

    }

    required convenience init(data: [String : AnyObject]) {
        self.init(json: JSON(data))
    }

}


/*
class User: NSObject {


     else if let errorDescription = json["error_description"].string {
         if errorDescription.uppercased().range(of: "EXPIRED") != nil {
             let userInfo: [String : Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("", value: errorDescription, comment: "")]
             let error = NSError(domain: "ParsingErrorDomain", code: ServicesErrorCodes.expiredPassword, userInfo: userInfo)
             completion(nil, error)
         }
         else {
             let userInfo: [String : Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("", value: errorDescription, comment: "")]
             let error = NSError(domain: "ParsingErrorDomain", code: ServicesErrorCodes.generic, userInfo: userInfo)
             completion(nil, error)
         }
     }
     else {
         let userInfo: [String : Any] = [NSLocalizedDescriptionKey :  NSLocalizedString("", value: Constants.Alert.genericError, comment: "")]
         let error = NSError(domain: "ParsingErrorDomain", code: ServicesErrorCodes.generic, userInfo: userInfo)
         completion(nil, error)
     }

}
*/
