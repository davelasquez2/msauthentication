//
//  MSAuthentication+Bundle.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/28/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import Foundation

// This class allows us to use the bundle init for class
class MSAuthClass {}

extension Bundle {

    // Returns BigModule target bundle
    public static var msauth: Bundle {
        return Bundle(for: MSAuthClass.self)
    }

}
