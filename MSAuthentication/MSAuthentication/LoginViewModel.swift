//
//  LoginViewModel.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/29/20.
//

import RxSwift
import RxCocoa
import SwiftyJSON

struct LoginViewModel {

    let email =  BehaviorRelay<String>(value: "")
    let password =  BehaviorRelay<String>(value: "")
    let forgotEmail =  BehaviorRelay<String>(value: "")

    func attemptToLogin(completion: @escaping((JSON?, Error?) -> ())) {
        LoginService.login(username:email.value, password: password.value) {
            (user, error) in
            completion(user,error)
        }
    }

    func attemptToForgotPassword(completion: ((JSON?, Error?) -> ())? = nil) {
        //TODO:
        print("FORGOT PASSWORD \(forgotEmail.value)")
    }
}
