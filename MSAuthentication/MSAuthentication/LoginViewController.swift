//
//  LoginViewController.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/28/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import UIKit

public class LoginViewController: UIViewController {

    // MARK: - IBOutlet properties
    // -----------------------------
    @IBOutlet weak var emailTextField: UITextField?
    @IBOutlet weak var passwordTextField: UITextField?

    @IBOutlet weak var passwordHelpButton: UIButton!

    @IBOutlet weak var registerButton: UIButton?
    @IBOutlet weak var forgotPasswordButton: UIButton?


    // MARK: - Configurable attributes
    public var hideRegisterButton: Bool =  true
    public var hideForgotButton: Bool =  true

    // MARK: - Model
    public var loginModel: LoginViewModel


    // MARK: - Protocol
    public var delegate: MSAuthenticationProtocol?

    // MARK: - Init
       // -----------------------------
/*    init?(coder: NSCoder, delegate: MSAuthenticationProtocol, loginModel: LoginViewModel ) {
           self.loginModel = loginModel
        self.delegate = delegate

           super.init(coder: coder)
       }

*/
    required init?(coder: NSCoder) {
        self.loginModel = LoginViewModel()
        super.init(coder: coder)
    }
    // MARK: - Lifecycle
    // -----------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        //socialContent.isHidden = true

        setupNavigationBar()
        setupTextChangeHandling()
        setupValidationRules()

        registerButton?.isHidden = self.hideRegisterButton
        forgotPasswordButton?.isHidden = self.hideForgotButton
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isHidden = false

        self.emailTextField?.text = ""
        self.passwordTextField?.text = ""

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = " "
        self.navigationController?.navigationBar.isHidden = false
    }


    //MARK: - RxSwift Setup
    // -----------------------------
    func setupTextChangeHandling(){
        emailTextField!
            .rx
            .text
            .orEmpty
            .bind(to: viewModel.email)
            .disposed(by: disposeBag)

        passwordTextField!
            .rx
            .text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: disposeBag)
    }

    //MARK: - IBActions
    // -----------------------------
    @IBAction func didTapLogin(_ sender: Any) {

       /* guard emailTextField!.validate() && passwordTextField!.validate() else {
            return
        }
*/
       // self.startLoading()

        viewModel.attemptToLogin(){ (response, error) in
          //  self.stopLoading()
            if let error = error {
                if let delegate = self.delegate {
                    delegate.loginDidFail(error: error)
                }
            }
            else if let response = response {
                if let delegate = self.delegate {
                    delegate.loginDidSuccess(response: response)
                }
            }
        }
    }

    @IBAction func didTapForgotPassword(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapForgotPassword()
        }
    }

    @IBAction func didTapCreateAccount(_ sender: Any) {
        if let delegate = self.delegate {
            delegate.didTapSignUp()
        }
    }

    @IBAction func didTapPasswordHelp(_ sender: Any) {
        //TODO:
        /*let vc = Storyboards.main.instantiateViewController(withIdentifier: PopupViewController.identifier) as! PopupViewController
        vc.popupTitle = Messages.passwordPoliciesTitle
        vc.popupMessage = Messages.passwordPoliciesCreateMessage
        router.presentDetailOver(vc, fromVC: self, navBarHidden: true, modal: nil)*/
    }



    // MARK: - Misc
    // ------------------
    func validateEmail(_ email: String) -> Bool{
        let test = NSPredicate(format: "SELF MATCHES %@", ValidatorDefaultRules.email)
        return test.evaluate(with: email)
    }

    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = UIColor.colorWithHexString(hex: Colors.charcoalGrey)
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    func setupValidationRules(){
      /*  emailTextField!.validationRules = [ErrorValidation(expression: ValidatorDefaultRules.email, message: Messages.invalidEmail)]

        passwordTextField!.validationRules =  [ErrorValidation(minimum: 1,
                                                               message: Messages.mandatoryField)]

        assert(emailTextField!.validationRules.count > 0,
               "Email rules was unexpectedly equal to 0")

        assert(passwordTextField!.validationRules.count > 0,
               "Password rules was unexpectedly equal to 0")*/

    }

}
