//
//  LoginService.swift
//  MSAuthentication
//
//  Created by Daniela Velasquez on 1/28/20.
//  Copyright © 2020 Daniela Velasquez. All rights reserved.
//

import Alamofire
import SwiftyJSON

protocol ServiceProtocol: AnyObject {
    func requestDidSuccess(response: [String: AnyObject])
    func requestDidFail(error: [String: Any]?)
}

class LoginService {

    static let instance = LoginService()

    public var delegate: ServiceProtocol?

    private var loginHeaders: [String:String]?

    // MARK: - Generic Errors
    static private let invalidURL: [String: Any] =  ["title": "Invalid login URL",
                                                     "details": "Invalid login URL",
                                                     "status": 0]

    private init() { }

    public class func login(username:String, password:String, completion: @escaping((JSON?, Error?) -> ())) {

        let requestParams:[String:Any] = ["username": username,
                                          "password": password]

        let request = Alamofire.request(MSAuthenticationSettings.URLs.login,
                                        method: .post, parameters: requestParams,
                                        encoding: JSONEncoding.default,
                                        headers: [:])

            request.responseJSON { response in
              switch response.result {
              case .success(let data):
                let json = JSON(data)
                let statusCode = response.response?.statusCode ?? 0
                if self.okResponse(statusCode) {
                    completion(json, nil)
                    //self.delegate?.requestDidSuccess(response: data as! [String: AnyObject])
                } else {
                    ompletion(nil, json)
                    //self.delegate?.requestDidFail(error: data as! [String: Any])
                }
              case .failure(let error):
                  completion(nil, error)
              }
          }
      }

    public func setHeaders(headers: [String: String]) {
        loginHeaders = headers
    }

    private func okResponse(_ statusCode: Int) -> Bool {
        if statusCode == 200 || statusCode == 201 {
            return true
        }

        return false
    }
}
